Untuk running server :
- ng server

Untuk download module :
- npm install

Soal Ujian :
<ul>
  <li>
    Buat tombol 'Simpan'.
    <br>
    Ketika tombol 'Simpan' ditekan :
    <br>
    - Data-data yang dimasukan akan tersimpan dan masuk ke table.
  </li>
  <li>
    Buat tombol 'Hapus' disetiap baris yang terdapat pada table.
    <br>
    Ketika tombol 'Hapus' ditekan :
    <br>
    - Data yang dipilih akan terhapus dari table.
  </li>
  <li>
    Buat tombol 'Ubah' disetiap baris yang terdapat pada table.
    <br>
    Ketika tombol 'Ubah' ditekan :
    <br>
    - Data yang dipilih untuk diubah akan berpindah ke form input.
    <br>
    - Muncul tombol 'Batal' untuk membatalkan pengubahan data.
    <br>
    - Perilaku tombol 'Simpan' akan berubah menjadi update data ketika ditekan.
  </li>
  <li>
    Ubah Input Text 'userId' menjadi Autocomplete / Dropdown / Input Text dengan Validasi User ID yang dimasukan harus terdaftar
  </li>
  <li>
    Tambahkan validasi 'required' untuk field 'userId' dengan message 'User ID harus diisi!'
    ketika user tidak memasukan nilai pada field 'userId'
  </li>
  <li>
    Tambahkan validasi 'required' untuk field 'title' dengan message 'Title harus diisi!'
    ketika user tidak memasukan nilai pada field 'title'
  </li>
  <li>
    Tambahkan validasi 'pattern' untuk field 'body' dengan message 'Nilai yang dimasukan harus alfanumeric!'
    ketika user tidak memasukan nilai dengan pola alfanumeric pada field 'body'
  </li>
  <li>
    Tambahkan Table untuk menampilkan daftar 'To Do' yang sudah ditambahkan
  </li>
</ul>