import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { CaseComponent } from './case/case.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  @ViewChild(CaseComponent)
  public child: CaseComponent;

  public todoform;
  public myName = 'Afianti Rosa';
  public listTodo: any[] = [];
  public unique: any;
  public api = 'https://jsonplaceholder.typicode.com/';
  public isEdit = false;
  public idItems = '';


  constructor(private fb: FormBuilder, private http: HttpClient) {
    this.todoform = this.fb.group({
      id: null,
      userId: ['', Validators.required],
      title: ['', Validators.required],
      body: ['', Validators.pattern('^[a-zA-Z0-9]*$')]
    });
  }

  public ngOnInit() {
    this.onLoad();
  }

  public toEdit(data: any) {
    this.todoform.patchValue({
      id: data.id,
      userId: data.userId,
      title: data.title,
      body: data.body
    });
    this.isEdit = true;
    this.idItems = data.id;
  }

  public onSubmit() {
    console.log(this.isEdit);
    if (this.isEdit === true) {
      this.http.put(this.api + 'posts/' + this.idItems , this.todoform.value).subscribe((data: any) => {
        console.log('A', data);
        this.onLoad();
      });
    } else {
      this.http.post(this.api + 'posts', this.todoform.value).subscribe((data: any) => {
        console.log(data);
        this.onLoad();
      });
    }
  }

  public onLoad() {
    this.http.get(this.api + 'posts').subscribe((data: any) => {
      this.listTodo = data;
      const idx = this.getUserId(data);
      this.unique = this.removeDuplicateUsingFilter(idx);
      console.log('this is a', this.unique);
    });
  }

  public getUserId(data) {
    var a = [];
    console.log(data);
    data.forEach(function (value) {
      a.push(value.userId);
    });
    return a;
  }

  public onRemove(id) {
    this.http.delete(this.api + 'posts/' + id).subscribe((data) => {
      console.log('A');
      this.onLoad();
    });
  }

  public batal() {
    this.todoform.patchValue({
      id: null,
      userId: '',
      title: '',
      body: ''
    });
    this.isEdit = !this.isEdit;
  }

  removeDuplicateUsingFilter(arr) {
    const unique_array = arr.filter(function(elem, index, self) {
        return index === self.indexOf(elem);
    });
    return unique_array;
  }

}
